<?php
class blibliothèque {
  private $db = NULL;
  private $title = NULL;
  private $author = NULL;
  private $publication = NULL;
  private $listBooks = NULL;

  public function __construct($db, $title, $author, $publication) {
      $this->setDb($db);
      $this->setTitle($title);
      $this->setAuthor($author);
      $this->setPublication($publication);
  }

   public function getTitle() {return $this->title;}
   public function getAuthor() {return $this->author;}
   public function getPublication() {return $this->publication;}
   public function getListBooks() {return $this->listBooks;}

   private function setDb(PDO $db){
     //on indique ede quel type sera l'arguement
     //ce renseignement,qu'on utilise uniquement dans ce cas
     // de type complexe (array, object)
     $this->db = $db;
     return $this->db;
   }
   public function setTitle($title) {
     $this->title = $title;
     return $this->title;
   }

   public function setAuthor($author) {
     $this->author = $author;
     return $this->author;
   }

   public function setPublication($publication) {
     $this->publication = $publication;
     return $this->publication;
   }

   public function generate() {
     $query = $this->db->prepare(
       'SELECT books.title, books.publication, authors.firstname, authors.lastname
        FROM books
        JOIN authors
        ON books.author = authors.id
       ');
      $query->execute();
//PDO = PHP Document Object
      $listBooks = $query->fetchAll(PDO::FETCH_OBJ);
      return $listBooks;
    }// NIQUE TA MERE GENERATE

}
    $listBooks = generate();
    var_dump($listBooks)
?>

<h2>Liste des livres</h2>
<table class="table table-bordered table-striped">
  <tr>
    <th>Titre</th>
    <th>Auteur</th>
    <th>Année de parution</th>
  </tr>
  <?php foreach ($listBooks as $listBook): ?>
    <tr>
      <td><?= $listBook->title ?></td>
      <td><?= $listBook->firstname .' '. $listBook->lastname?></td>
      <td><?= $listBook->publication ?></td>
    </tr>
<?php endforeach; ?>
</table>
