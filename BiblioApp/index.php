<?php
include('routes.php');
$db = new PDO('mysql:host=localhost;dbname=BiblioApp', 'root', 'paris');
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
if (isset($_GET['route'])) {
  $route = $_GET['route'];
}

?>

<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title>BiblioApp</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  </head>
  <body>
    <nav>
      <ul class="nav nav-tabs">
        <li><a href="?route=list">Blibliothèque</a></li>
        <li><a href="?route=actions/add_book">Ajouter un livre</a></li>
        <li><a href="?route=actions/add_author">Ajouter un auteur</a></li>
      </ul>
    </nav>
    <?php

    if(isset($route)) {
      include($routes[$route]);
    }
    ?>
  </body>


</html>
