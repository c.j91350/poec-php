<?php
$routes = array(
  'list'                 => 'list.php',
  'actions/add_book'     => 'actions/add_book.php',
  'actions/add_author'   => 'actions/add_author.php',
  'actions/delete'       => 'actions/delete.php',
  'class/Author'         => 'class/Author.php',
  'class/Book'           => 'class/Book.php'
);
?>
