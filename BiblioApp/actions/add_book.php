<?php
  if(isset($_POST['submitbook'])) {
    $cond1 = $_POST['title'] != "";
    $cond2 = $_POST['isbn'] != "";
    $cond3 = $_POST['nb_pages'] != 0;
    $cond4 = $_POST['nb_pages'] != "";
    $cond5 = $_POST['publication'] != 0;
    $cond6 = $_POST['publication'] != "";

     if($cond1 && $cond2 && $cond3 && $cond4 && $cond5 && $cond6) {
       $query = $db->prepare(
         'INSERT INTO books (title, isbn, nb_pages, publication)
          VALUES (:title, :isbn, :nb_pages, :publication)
          JOIN authors
          ');

       $result = $query->execute(array(

         //':id' => $_POST['id'],
         ':title' => $_POST['title'],
         ':isbn' => $_POST['isbn'],
         ':nb_pages' => $_POST['nb_pages'],
         ':publication' => $_POST['publication']
       )); var_dump($_POST);


       ($result)
       ? header('location:?route=list')
       : print('l\'enregistrement a echoué');

     } else {
       echo "Merci de renseigner correctement tous les champs.";
     }
  } //fin de if isset
  $query2 = $db->prepare(
    'SELECT books.id, authors.firstname, authors.lastname,
     FROM books
     JOIN authors
     ON books.id_author = authors.id
     ');

  $result2 = $query2->execute();

  $authors = $query2->fetchAll(PDO::FETCH_OBJ);


 ?>

<h2>Ajouter un livre</h2>
<form method="POST">
  <div class="form-group">
    <label for="title">Titre : </label>
    <input type="text" name="title">
  </div>
  <div class="form-group">
    <label for="nbpages">Nombre de pages : </label>
    <input type="number" name="nb_pages">
  </div>
  <div class="form-group">
    <label for="isbn">ISBN : </label>
    <input type="text" name="isbn">
  </div>
  <div class="form-group">
    <label for="publication">Année de parution : </label>
    <input type="text" name="publication">
  </div>
  <div class="form-group">
    <select name="find_author">
      <option value="0">Sélectionner un auteur</option>
      <?php foreach ($authors as $author): ?>
        <option value="<?=$author->id?>"><?=$author->firstname . ' ' . $author->lastname?></option>
      <?php endforeach; ?>



    </select>
  </div>
    <!-- <input type="hidden" name="id"> -->
    <input type="submit" name="submitbook" value="Enregistrer">
</form>
