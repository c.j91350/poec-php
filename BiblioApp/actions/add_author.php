<?php
if(isset($_POST['submitauthor'])) {
  $cond1 = $_POST['firstname'] != "";
  $cond2 = $_POST['lastname'] != "";
  $cond3 = $_POST['birthyear'] != "";
  $cond4 = $_POST['birthyear'] != 0;
  $cond5 = $_POST['country'] != "";

    if($cond1 && $cond2 && $cond5) {
      $query = $db->prepare(
        'INSERT INTO authors (firstname, lastname, birth_year, country)
        VALUES (:firstname, :lastname, :birthyear, :country)
        ');
        $result = $query->execute(array(
          ':firstname' => $_POST['firstname'],
          ':lastname' => $_POST['lastname'],
          ':birthyear' => $_POST['birthyear'],
          ':country' => $_POST['country']
        ));
        ($result)
        ? header('location:?route=actions/add_book')
        : print('L\'enregistrement a échoué');
    } else {
      echo 'Merci de renseigner correctement tous les champs.';
    }
} // Fin de if isset



?>
<h2>Ajouter un auteur</h2>
<form method="POST">
  <div class="form-group">
    <label for="firstname">Prénom : </label>
    <input type="text" name="firstname">
  </div>

  <div class="form-group">
    <label for="lastname">Nom : </label>
    <input type="text" name="lastname">
  </div>

  <div class="form-group">
    <label for="birthyear">Année de naissance : </label>
    <input type="text" name="birthyear">
  </div>

  <div class="form-group">
    <label for="country">Pays : </label>
    <input type="text" name="country">
  </div>

  <input type="submit" name="submitauthor" value="Enregistrer">
</form>
