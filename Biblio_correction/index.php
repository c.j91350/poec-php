<?php
include('routes.php');
$db = new PDO('mysql:host=localhost;dbname=biblio', 'root', 'paris');
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
if (isset($_GET['route'])) {$route = $_GET['route'];}
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>correction TP 3 : biblio App</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  </head>
  <body>
    <h1>Correction TP 3 : biblio App</h1>
    <nav>
      <ul class="nav nav-tabs">
        <li><a href="?route=books">Liste des livres</a></li>
        <li><a href="?route=authors">Liste des auteurs</a></li>
      </ul>
    </nav>
  <?php
  if(isset($route)) {include($routes[$route]);}
  ?>
  </body>
</html>
