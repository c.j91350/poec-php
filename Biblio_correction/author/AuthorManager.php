<?php
 class AuthorManager {
   //cette classe sert à faire la connexion avec la BD (c'est un intermediaire)
   //s'occupe des opérations de CRUD
   private $db;

   public function __construct($db) {
     $this->setDb($db);

   }

   public function getDb(){return $this->db;}
   public function setDb($db) {
     $this->db = $db;
     return $this->db;
   }

   public function save(Author $author) {
     // $author deja hydraté avant à l'istanciation contient deja des données
     $query = $this->db->prepare(
       'INSERT INTO author (firstname, lastname, birth_year, country)
        VALUES (:firstname, :lastname, :birth_year, :country)
        ');

     $result = $query->execute(array(
       ':firstname' => $author->getFirstName(),
       ':lastname' => $author->getLastName(),
       ':birth_year' => $author->getBirthYear(),
       ':country' => $author->getCountry()
     ));

     return $result;
   }

   public function list(){
     $query = $this->db->prepare('SELECT * FROM author ORDER BY lastname ASC');
     $query->execute();
     $results = $query->fetchAll(PDO::FETCH_OBJ);
     $authors = array();
     //transformation des résultats SQL e objet de type Author
       foreach ($results as $r) {
         //a chaque intération nous créons un objet author
         $author = new Author($r->firstname, $r->lastname, $r->birth_year, $r->country);
         //nous renseignons egalement lid de l'auteur via un setter approprié
         $author->setId($r->id); // a cet étape hydratation est complète
         //ajout de l'author dans le tableau
         array_push($authors, $author);
       }


     return $authors; // return un tableau d'objet  de type author
   }

   public function deleteById($id) {
     $query = $this->db->prepare('DELETE FROM author WHERE id = :id');
     $result = $query->execute(array(':id' => $id ));
     return $result;
   }

   public function getById($id) {
     $query = $this->db->prepare('SELECT * FROM author WHERE id = :id');
     $query->execute(array(':id' => $id));
     $result = $query->fetch(PDO::FETCH_OBJ);

     //transformation de l'objet PDO $result en objet de type Author
     $author = new Author(
       $result->firstname,
       $result->lastname,
       $result->birth_year,
       $result->country
     );
     $author->setId($result->id); //hydratation complète
     return $author;
   }
   public function getBooks($author_id){
     //a faire
     //renvoi un tableau d'objet de type Book
   }
   }

?>
