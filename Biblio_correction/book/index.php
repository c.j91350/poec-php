<?php
include_once('./author/Author.php');
include_once('./author/AuthorManager.php');

include_once('./book/Book.php');
include_once('./book/BookManager.php');
 $author_manager = new AuthorManager($db);  //gestionnaire d'auteur
 $authors = $author_manager->list();
 $book_manager =new BookManager($db); //gestionnaire de livre


 if(isset($_POST['submit'])) {
   //creation d'un objet Book

   $book = new Book(
     $_POST['title'],
     $_POST['isbn'],
     intval($_POST['nb_pages'])
   );
   //a partir d'un identitifant  d'auteur, je souhaite
   //obtenir un objet Author complet
   $author = $auteur_manager->getById(intval($_POST['author']));
   $book->setAuthor($author);
   //variante possible : au lieu de reconstituer un objet Author complet
   // à partir d'un identifiant, on aurait pu traiter la propiété
   //author de l'objet book en tant integer
   //
   //fournir cet objet au BookManager
   if($book_manager->save($book) == 0) echo "L'enregistrement du livre à échouer";
 }
?>

<h2>Livres</h2>

<form method="POST" class="form-inline">

  <div class="form-group">
    <input type="" name="title" placeholder="Titre">
  </div>

  <div class="form-group">
    <input type="text" name="title" placeholder="Nom">
  </div>

  <div class="form-group">
    <input type="text" name="isbn" placeholder="isbn">
  </div>

  <div class="form-group">
    <input type="number" name="nb_pages" placeholder="Nombre de page">
  </div>

  <div class="form-group">
    <select name="author">
      <option value="0">Sélectionner un auteur :</option>
      <?php foreach ($authors as $author): ?>
        <option value="<?= $author->getId ?>"> <?= $author->getLastName() ?></option>
      <?php endforeach; ?>
    </select>
  </div>
    <input type="submit" name="submit" value="Enregistrer">
</form>

<!-- Liste des livre -->
<table class="table table-striped table-bordered">
  <tr>
    <th>Titre</th>
    <th>ISBN</th>
    <th>Nombre de pages</th>
    <th>auteur</th>
    <th>actions</th>
  </tr>
<?php foreach ($authors as $author): ?>
  <tr>
    <td><?= $author->getFirstName() ?></td>
    <td><?= $author->getLastName() ?></td>
    <td><?= $author->getBirthYear() ?></td>
    <td><?= $author->getCountry() ?></td>
    <td><a href="?route=authors&action=delete&id=<?=$author->getId()?>" class="btn btn-danger btn-xs">Supprimer</a></td>
  </tr>
<?php endforeach; ?>
