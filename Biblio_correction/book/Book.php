<?php
class Book {
  private $id;
  private $title = NULL;
  private $isbn = NULL;
  private $nb_pages = NULL;
  private $author = NULL;

  public function __construct($title, $isbn, $nb_pages) {
    $this->setTitle($title);
    $this->setIsbn($isbn);
    $this->setNbPages($nb_pages);
  }

  public function getId(){return $this->id;}
  public function getTitle(){return $this->title;}
  public function getIsbn(){return $this->isbn;}
  public function getNbPages(){return $this->nb_pages;}
  public function getAuthor(){return $this->author;}

  public function setIdBook($id) {
    $this->id = $id;
    return $this->id;
  }
  public function setTitle($title) {
    $this->title = $title;
    return $this->title;
  }
  public function setIsbn($isbn) {
    $this->isbn = $isbn;
    return $this->isbn;
  }
  public function setNbPages($nb_pages) {
    $this->nb_pages = $nb_pages;
    return $this->nb_pages;
  }
  public function setAuthor(Author $author) {
    $this->author = $author;
    return $this->author;
  }
}
?>
