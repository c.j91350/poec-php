<?php

class BookManager {
   private $db;

   public function __construct($db) {
     $this->setDb($db);
   }

   public function getDb(){return $this->db;}
   public function setDb($db) {
     $this->db = $db;
     return $this->db;
   }
   public function list(){
     //à faire
   }
   public function deleteById($id){
     // a faire
   }

   public function save(Book $book) {
     $query = $this->db->prepare(
       'INSERT INTO book (title, isbn, nb_pages, author)
        VALUES (:title, isbn, :nb_pages, :author)
       ');

       //afin d'etre en conformié avec le données attendu par SQL
       //(la colonne book.author attend un int), on execute
       //la méthode getId() de l'objetAuthor renvoyé par $book->getAuthor()
    $result = $query->execute(array(
      ':title' => $book->getTitle(),
      ':isbn' => $book->getIsbn(),
      ':nb_pages' => $book->getNbPages(),
      ':author' => $book->getAuthor()->getId() // renvoi un objet complet
    ));
    return $result;
   }
}
?>
