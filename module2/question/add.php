<?php
include('./categories.php'); // accès à la variable $categories
include('./levels.php');
$categories = getCategories($db);

  if (isset($_POST['submit'])) {
    //var_dump($_POST);
    // 1/ Validation des données
    $cond1 = $_POST['title']        != "";
    $cond2 = $_POST['category']     != "0";
    $cond3 = $_POST['level']        != "0";


    //var_dump($cond3);
    if ($cond1 && $cond2 && $cond3) {
      // toutes les conditions sont remplies
      // 2/ Enregistrement des données dans la DB
          // 1. preparation de la requête
      $query = $db->prepare(
        'INSERT INTO question (title, category, level) VALUES (:title, :category, :level)'
      );
          // 2.Execution
    $result = $query->execute(array(
        ':title'    =>  $_POST['title'],
        ':category' =>  intval($_POST['category']),
        ':level'    =>  intval($_POST['level'])
        //intval converti une valeur en entier
      ));
              if ($result){
              // echo "Enregistrement reussi";
              header('location:?route=question/list');
              //header est function qui redirige l'utilisateur vers une autre page
              } else {
              echo "Enregistrement échoué";
              }
        } else {
          echo "Une des condition pas remplie";
        }
}
 ?>

<h2>Ajout d'une question</h2>
<form style="width:30%" class="" action="" method="POST">
  <div class="form-group">
    <label for="">Intitulé
      <input type="text" class='form-control' name="title" value="">
    </label>
  </div>

  <div class="form-group">
    <select name="category">
      <option value="0">Choisir une catégorie :</option>
        <?php foreach ($categories as $category): ?>
          <option value="<?=$category->id?>"><?= $category->category ?></option>
        <?php endforeach; ?>
    </select>
  </div>

  <div class="form-group">
    <select name="level">
      <option value="0">Choisir un niveau de difficulté</option>
        <?php foreach ($levels as $key => $level): ?>
          <option value="<?= $key ?>"><?= $level ?></option>
        <?php endforeach; ?>
    </select>
  </div>



  <input type="submit" class="btn btn-primary" name="submit" value="Enregistrer">
</form>
