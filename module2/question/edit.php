<?php
include('./categories.php'); // accès à la variable $categories
include('./levels.php'); // accès à la variable $levels;

$categories = getCategories($db);
if (isset($_GET['id'])) {
  $id = $_GET['id'];
  // echo "$id";

  $query = $db->prepare('SELECT * FROM question WHERE id = :id');
  $query->execute(array(
    ':id' => intval($id)
  ));
  $question = $query->fetch(PDO::FETCH_OBJ); // renvoi un objet il converti la ligne sql en structure php (un objet) on l'utilisa quand on a besoin d'affiche des lignes sql
}
// Cas ou le formulaire de mise à jour est envoyé
if (isset($_POST['submit']))  {
  //1 Validation des données
  $cond1 = $_POST['title']        != ""; // les clés proviennent des attributs name <input name="">
  $cond2 = $_POST['category']     != "0";
  $cond3 = $_POST['level']        != "0";


  if ($cond1 && $cond2 && $cond3) {
    //2 enrigistrement des nouvelles données
    $query = $db->prepare(
      //1. preparation
      'UPDATE question SET title = :title, category = :category, level = :level
       WHERE id= :id'
      );
          $result = $query->execute(array(
            ':title' => $_POST['title'],
            ':category' => intval($_POST['category']),
            ':level' => intval($_POST['level']),
            ':id' => intval($_POST['id']),
          ));
          if ($result) {
            header('location:?route=question/list');
          } else {
            echo "<p> Mise à jour non effectuée </p>";
          }

  } else {
    echo "Une des condition n'est pas remplie";
  }
}
 ?>
 <form style="width:30%" class="" action="" method="POST">
   <div class="form-group">
     <label for="">Intitulé
       <input value="<?=$question->title ?>" type="text" class='form-control' name="title" value="">
     </label>
   </div>

   <div class="form-group">
     <select name="category">
       <option value="0">Choisir une catégorie</option>
      <?php foreach ($categories as $category):?>
            <?php if ($question->category == $category->id): ?>
                  <option value="<?=$category->id?>" selected><?=$category->category?></option>
            <?php else: ?>
                  <option value="<?=$category->id?>"><?=$category->category?></option>
            <?php endif; ?>
      <?php endforeach; ?>
     </select>
   </div>

   <div class="form-group">
     <select class="" name="level">
       <option value="0">Choisir un niveau de difficulté</option>
       <?php foreach ($levels as $key => $level): ?>
        <?php if ($question->level == $key): ?>
             <option value="<?= $key ?>" selected> <?=$level ?> </option>
        <?php else: ?>
             <option value="<?= $key ?>"> <?=$level ?> </option>
        <?php endif; ?>

       <?php endforeach; ?> ?>
     </select>
   </div>

   <!--le champs hidden permet de ajout dans la $_POST des info que l'on souhaite conservées (ice c'est id)   -->
   <input type="hidden" name="id" value="<?=$id?>">
   <input type="submit" class="btn btn-primary" name="submit" value="Mettre à jour">
 </form>
