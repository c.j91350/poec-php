<?php
include('levels.php');
//Function retounant le nombre de réponses associés à une question - SQL renvoi le nombre de ligne
  function getNbAnswers($db, $id_question){
    $query = $db->prepare(' SELECT COUNT(*)
                            FROM answer
                            WHERE id_question = :id_question
    ');
    $query->execute(array(
      ':id_question' => $id_question
    ));
    // FETCH renvoir un tableau d'un seul élément (indice 0)
    $num = $query->fetch(PDO::FETCH_NUM);

    return $num[0];
  }

  // 1.prepation de la requête
 //$query = $db->prepare('SELECT * FROM question ORDER BY id DESC');
$query = $db->prepare(
  'SELECT question.id, question.title, question.level, categories.category AS category
   FROM question
   JOIN categories ON question.category = categories.id
   ORDER BY question.id DESC
');
 // 2. éxécution
 $query->execute();

 // 3. recuperation des données (fetch)
 $questions = $query->fetchAll(PDO::FETCH_OBJ);

 // var_dump($questions);

?>

<h2>Liste des questions</h2>
<table class="table table-bordered table-striped">
<tr>
  <th>#</th>
  <th>Intitulé</th>
  <th>Catégorie</th>
  <th>Niveau</th>
  <th>Actions</th>
</tr>
<!-- variante de la boucle foreach -->
<?php $i = 0; ?>
<?php foreach ($questions as $question):  ?>
  <tr>
    <td><?= ++$i; ?></td>
    <td><?= $question ->title ?></td>
    <td><?= $question ->category ?></td>
    <td><?= getLevelName($levels, $question->level) ?></td>
    <td>
      <a href="?route=answer/manage&id_question=<?= $question ->id?>"
      class="btn btn-info btn-xs">
        <?= getNbAnswers($db, $question->id)?> réponse(s)
      </a>
      <a href="?route=question/edit&id=<?= $question ->id?>"
      class="btn btn-warning btn-xs">Modifier</a>
      <a href="?route=question/delete&id=<?= $question ->id?>"
      class="btn btn-danger btn-xs">Supprimer</a>
        <!-- la balise <a> genere un requete en GET et il y a pas moyen de faire une requete en FORM hors d'un <form> -->
    </td>
  </tr>
<?php endforeach ?>
</table>
