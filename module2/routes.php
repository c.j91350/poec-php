<?php
$routes = array(
  'question/list' => 'question/list.php',
  'question/add' => 'question/add.php',
  'question/delete' => 'question/delete.php',
  'question/edit' => 'question/edit.php',
  'answer/manage' => 'answer/manage.php',
  'answer/delete' => 'answer/delete.php',
  'categories/manage' => 'categories/manage.php',
  'categories/delete' => 'categories/delete.php',
  'qcm' =>'qcm/index.php'
 );
?>
