<?php
  //   $categories =
  //   array('Politique',
  //   'Sport',
  //   'Divers',
  //   'Programmation',
  //   'Litterature',
  //   'Cuisine',
  //   'Histoire',
  //   'Cinéma',
  // );

function getCategories($db) {
  // $db est un objet de type PDO fournit en entree
  $query = $db->prepare(
    'SELECT * FROM categories ORDER BY id ASC');
  $query->execute();
  $categories = $query->fetchAll(PDO::FETCH_OBJ);

  return $categories;
}
  ?>
