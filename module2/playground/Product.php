<?php

class Product {
  //consante de classe acces seulement en lecture
    const CONSTANTE_DE_CLASSE = 'Je suis constante de classe';

    private $price = NULL;
    private $available = false;
    private $name = NULL;

    public $test = 'public test product';
    protected $test2 = 'publict est product';

    //constructeur
    public function __construct($name){
      //appel de la méthode setName en interne à l'instanciation
      $this->setName($name);
    }

    //accesseur (getter) => accès en lecture
    public function getPrice(){
      return $this->price;
    }
    public function getAvailable(){
      return $this->available;
    }
    public function getName(){
      return $this->name;
    }

    //mutateur (setter) => accès en ecriture
    public function setPrice($price) {
      $this->price = $price;
      return $this->price;
    }
    public function setAvailable($available) {
      $this->available = $available;
      return $this->available;
    }
    public function setName($name) {
      $this->name = $name;
      return $this->name;
    }





  }

?>
