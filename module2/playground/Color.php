<?php
class Color {
  public $colorHuman = NULL;
  const DEFAULT_COLOR = 'black';
  private $colors = array(
    array(
      'human' => 'red',
      'hexa' => '#FFOOOO',
      'rgb' => '255,0,0'
    ),
    array(
      'human' => 'green',
      'hexa' => '#00FFOO',
      'rgb' => '0,255,0'
    ),
    array(
      'human' => 'black',
      'hexa' => '#000000',
      'rgb' => '0,0,0'
    ),
  );
  //constructeur:  une function gérée automatiquement
  //lors de l'instanciation d'un objet
  //'new' le constuctuer permet de fournir à lobjet des données dès l'instanciation

  public function __construct($colorHuman) {
      //hydratation:"alimente" dès l'instanciation, fournit des données
      //au constructeur au moment de l'instanciation
      $this->colorHuman = $colorHuman;
      if($this->checkColor($colorHuman)){
        //couleur donnée à l'instanciation trouvée par la méthode privée checkColor
        //si vrai checkColor =>hydratation
        $this->colorHuman = $colorHuman;
      }
      else {
        //l'operateur 'self' permet de cibler (résoudre) la constante de classe
        //DEFAULT_COLOR. il cherche dans toute la classe
        $this->colorHuman = self::DEFAULT_COLOR;
      }
  }

  private function checkColor($colorStr){
    $colorHumanFound = false;
    foreach ($this->colors as $color) {
      if($color['human'] == $colorStr) {
        $colorHumanFound = true;
        break;
      }
    }
  }

  public function convertToHexa(){
    $colorHexa = NULL;
    foreach ($this->colors as $color) {
      if($color['human'] == $this->colorHuman){
        $colorHexa = $color['hexa'];
        break;
      }
    }
    return $colorHexa;
  }

  public function convertToRgb(){
    $colorRgb = NULL;
    foreach ($this->colors as $c) {
      if($color['human'] == $this->colorHuman){
        $colorRgb = $color['rgb'];
        break;
      }
    }
    return $colorRgb;
  }

}
?>
