<?php
include_once('Product.php');

class Book extends Product {
  //private = inaceessible depuis l'ext de la classe,
  //accessible seulement à l'interieur de la classe
  //non "ecrasable par une classe heritiere"
  //si la classe fille defini un propriete portant le meme nom que la classe mere
  //=> un doublon est genere

  //protected = inacessible depuis l'ext mais ecrasable par une classe heritière
  //si la classe fille defini un propriete portant le meme nom que la classe mere
  //=> cette propriete remplace celle provenant de la classe mère
  //ne s'utilise qu'en cas d'heritage

  //public = accessible depuis l'ext, ecrasable par une classe fille(heritiere)
  //si la classe fille defini un propriete portant le meme nom que la classe mere
  //=> cette propriete remplace celle provenant de la classe mère

   private $nbPages = NULL;


  public $test = 'protected test book';
  protected $test2 = 'pretected test book';

  public function __construct(){
    //le contructeur d'une classe fille ecrase celui de la classe parent
    // il ne y avoir qu'un seul constructeur par classe
    // les information du constructeur ne sont pas prise en compte dans l'objets
  }
  public function getTest2(){
    return $this->test2; // renvoir la valeur de la propriete protegee
  }

  public function getNbPages() {
    return $this->nbPages;
  }

  public function setNbPages($nbPages) {
    $this->nbPages = $nbPages;
    return $this->nbPages;
  }
}
?>
