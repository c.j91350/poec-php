<?php

function listeStagiaires() {
  $stagiaires = array(
    array(
    "id" => 1,
    "nom" => "abecassis",
    "prenom" => "stephane",
    "password" => "azerty",
    "totem" => "images.jpeg",
    "notes" => array(20, 13, 19)),

    array(
    "id" => 2,
    "nom" => "chauvet",
    "prenom" => "stevens",
    "password" => "azerty",
    "totem" => "artiste.jpeg",
    "notes" => array(9, 19, 16)),

    array(
    "id" => 3,
    "nom" => "grivel",
    "prenom" => "sebastien",
    "password" => "azerty",
    "totem" => "c.png",
    "notes" => array(5, 7, 11)),

    array(
    "id" => 5,
    "nom" => "vautour",
    "prenom" => "jessy",
    "password" => "azerty",
    "totem" => "c.png",
    "notes" => array(16, 18, 20)),

    array(
    "id" => 6,
    "nom" => "dupond",
    "prenom" => "eric",
    "password" => "azerty",
    "totem" => "c.png",
    "notes" => array(4, 9, 16)),

    array(
    "id" => 7,
    "nom" => "camara",
    "prenom" => "moussa",
    "password" => "azerty",
    "totem" => "c.png",
    "notes" => array()),

    array(
    "id" => 8,
    "nom" => "rerolle",
    "prenom" => "lea",
    "password" => "azerty",
    "totem" => "lea.jpeg",
    "notes" => array(15, 11, 18)),

    array(
    "id" => 9,
    "nom" => "dufour",
    "prenom" => "christophe",
    "password" => "azerty",
    "totem" => "c.png",
    "notes" => array(20, 20)),

    array(
    "id" => 10,
    "nom" => "rakoto",
    "prenom" => "christiane",
    "password" => "azerty",
    "totem" => "c.png",
    "notes" => array()),


  );
  return $stagiaires;
}

function stagiairesById($id) {
  //@ IN (entrée) integer
  //@ OUT (sortie) array || NULL
  $stagiaire = NULL;
  foreach (listeStagiaires() as $s) {
    if ($s['id'] == $id) {
      $stagiaire = $s;
      break; // signifie sortie de boucle si l'id est trouvé
    }
  }
  return $stagiaire;
  // $stagiaires = listeStagiaires(); appel de la fonction listeStagiaires()

}
?>
