$(document).ready(function() {
  //DOM chargé

  var app = {
    server: 'http://localhost:8000',
    data: {
      fruits: null,
    }
  };


  //ciblage et mise en cache
    var appHtml       = $('div#app');
    var btnTestAjax   = appHtml.find('button#btnTestAjax');
    var btnListFruits = appHtml.find('button#btnListFruits');
    var fruitDisplay  = appHtml.find('div#fruitDisplay');
    var selectFormat  = appHtml.find('select#selectFormat');
    var fruitDetails   = appHtml.find('div#fruitDetails');
    var cbComestible  = appHtml.find('input#cbComestible');
    var cbNonComestible  = appHtml.find('input#cbNonComestible');
    var search  = appHtml.find('input#search');
    var like = appHtml.find('i.like');


  //*** FONCIONS ***
    function init() {
      ajaxListFruits(); // appel la fonction de récuperation  de ruits
    }

    var ajaxFn = function() {
      $.get(app.server + '/fruits/api/json', function(res) {
        console.log(res);
        console.log(typeof res);
        //la réponse du server (res) est une chaîne de caractère
        // au format JSON
        var fruit = JSON.parse(res);
        console.log(fruit);
        console.log(typeof fruit);
        console.log('Nom de fruit : ' + fruit.name);

        var fruitData = 'Nom:' + fruit.name;
        fruitData += '<br/>' + 'Origin:' + fruit.origin;
        fruitDisplay.html(fruitData);
      });
    }

    var ajaxListFruits = function() {
      if (!app.data.fruits) {
          //si les données ont déja été stockée on les demande au serveur
        $.get(app.server + '/fruits/api/list', function(res) {
          // les requetes ajax sont asychrones
          // il faut s'assurer que la réponse du serveur est reçu
          //avant d'affectuer des opérations basées sur la réponse du serveure
          var fruits = JSON.parse(res);
          //stockage de la réponse du serveur
          app.data.fruits = fruits;

          fruitDisplay.html(transformToHtml(fruits, selectFormat.val() ));
          });
        } else {

          // les données sont deja reçue
          fruitDisplay.html(transformToHtml(app.data.fruits, selectFormat.val() ));
          //écouteur d'évenement bien placé: on cible td.fruit existera dans le DOM
          // $(td.fruitName).click(function(){
          //   console.log('ok');
          // });
        }//fin de if
    }

    var transformToHtml = function(fruits, type) {
      var output = '';
      if (type == 'list'){
        output += '<ul>';
          //intération sur fruits
        fruits.forEach(function(fruit){
          output += '<li class="fruitName" id="'+fruit.id+'">' + fruit.name + '</li>';
        });
        output += '</ul>';
      } //IF TYPE LIST

      if (type == 'table') {
        output += '<table class="table table-bordered table-striped">';
        output += '<th>'+ 'Nom' + '</th>';
        output += '<th>'+ 'Origine' + '</th>';
        output += '<th>'+ 'Comestible' + '</th>';
        output += '<th>'+ 'Producteur' + '</th>';
        output += '<th>'+ 'Like' + '</th>';

        fruits.forEach(function(fruit){
            output += '<tr>';
            output += '<td class="fruitName" id="'+fruit.id+'">' + fruit.name + '</td>';
            output += '<td>' + fruit.origin + '</td>';
            (fruit.comestible == 1)
            ? output += '<td>' + 'Oui' + '</td>'
            : output += '<td>' + 'Non' + '</td>';

            (fruit.producer)
            ? output += '<td>' + fruit.producer + '</td>'
            : output += '<td>' + '' + '</td>';

            output += '<td class="like" id="'+ fruit.id +'">' + '<i class="fa fa-thumbs-o-up" aria-hidden="true"></i> ('+ fruit.like +') </td>';
            output += '</tr>';
        });
        output += '</table>';
      }
      return output;
    }

    var detailFruit = function() {
      /*
        La functionst .on() permet d'afficher un écouteur d'évenement à l'élément ciblé
        ou à un élément enfant (présent ou à venir dans le DOM)
         ici : lorsque td.fruitName appraîtra dans le DOM en tant que descendant
         de fruitDisplay, un écouteur d'évènement click lui sera affiché
      */
        var id = $(this).attr('id'); // récupération de la valeur associée
        // à l'attribut html (ici: id) est ciblé
        var url = app.server + '/fruits/api/detail/'+ id;

          $.get(url, function(res){
            var fruit = JSON.parse(res); //transformation en objet
            console.log(fruit);
            displayDetailFruit(fruit);
          });
    }

    var displayDetailFruit = function(fruit) {
      var output = '';
      var output = '<h4>' + fruit.name +' ('+ fruit.origin +')</h4>';

      if (fruit.producer) {
      output += '<p>Produit par '+fruit.producer.name+'</p>';
        if (fruit.producer.logo) {
          var url = app.server + '/img/logo/'   + fruit.producer.logo;
          output += '<img class="logo" alt="" src="'+url+'">';
        }
      }
      fruitDetails.html(output);
    } //fin de fonction

    var filterByComestible = function(fruit) {
      var comestibleVisible = cbComestible.prop("checked");
      var nonComestibleVisible = cbNonComestible.prop("checked");

      //si la case comestible n'est pas coché (checked = false)
      //Nous devons retire du tableau tous fruits comestible
      //.fruitssans cibler le dom, sans itérer le Dom (couteux)
      // la meilleur solution consiste d'abord filterByComestible
      //la source de données (app data.fruits)

       var fruitFiltered = app.data.fruits.filter(function() {
         if ( comestibleVisible  &&  nonComestibleVisible)  return true;
         if (!comestibleVisible &&  !nonComestibleVisible)  return false;
         if ( comestibleVisible  && !nonComestibleVisible)  return fruit.comestible;
         if (!comestibleVisible &&   nonComestibleVisible)  return !fruit.comestible;
       });

       fruitDisplay.html(transformToHtml(fruitFiltered, 'table'));
    }

    var filterByKeyword = function(e) {
      //console.log(e.key); permet de savoir sur quelle touche du clavier on tape
      var input = $(this).val();
      var fruitFiltered =
        app.data.fruits.filter(function(fruit) {
          var test = fruit.name.toLowerCase().indexOf(input.toLowerCase()) !== -1;
              test += fruit.origin.toLowerCase().indexOf(input.toLowerCase()) !== -1;
          return test;
        });
        //mise à jour du DOM en fonction du filtrage à la saisie du clavier
        fruitDisplay.html(transformToHtml(fruitFiltered, 'table'));
    }

    var ajaxPost = function() {
      var url = app.server + '/fruits/api/post';
      // donnée envoyées au serveur automatiquement converties en JSON
      var data = {id: 88, name: search.val(),
        sport: [
          {id: 1, name: "Football"},
          {id: 2, name: "Basket-ball"},
        ]
      };
      $.post(url, data, function(res) {
        console.log(res);
      });
    }

    var incrementLikePost = function() {
      var identifiant = $(this).attr("id");
      var data = {id: identifiant};
      var url = app.server + '/fruits/api/like';

      $.post(url, data, function(res) {
        console.log(res);
      });

    }



//******************* FIN DE ZONE DE FONCTIONS ****************

  //*** évenement ***

  btnTestAjax.click(ajaxPost);
  btnListFruits.click(ajaxListFruits);
  selectFormat.change(ajaxListFruits);
  fruitDisplay.on('click', '.fruitName', detailFruit);
  //problème de chronologie ce code sera valable dans le futur
  //càd lorsque la balise <td> existera dans le DOM
  // $(td.fruitName).click(function(){
  //   console.log('ok');
  // });
  cbComestible.on('click', filterByComestible);
  cbNonComestible.on('click', filterByComestible);
  search.on('keyup', filterByKeyword);
  fruitDisplay.on('click', '.like', incrementLikePost);

//******************* FIN DE ZONE D'EVENEMENTS ****************

init();
}); //fin de ready
