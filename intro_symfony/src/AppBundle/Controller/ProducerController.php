<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use AppBundle\Entity\Producer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
/**
 * @Route("/producer")
 */
class ProducerController extends Controller
{
    /**
     * @Route("/index", name="producer_index")
     */
    public function indexAction()
    {
        $producers = $this->getDoctrine()
          ->getRepository(Producer::class)->findAll();

        return $this->render('AppBundle:Producer:index.html.twig', array(
          'producers' => $producers
        ));
    }

    /**
     * @Route("/add")
     */
    public function addAction(Request $req)
    {
        //creation d'un objet vide
      $producer = new producer();
        //la méthode createFormBuilder() permet de créer un form en pur PHP OO, renvoi un objet de type form builder
      $form = $this->createFormBuilder($producer)
        ->add('name', TextType::class)
        ->add('email', EmailType::class)
        ->add('logo', FileType::class, array('label' => 'Sélectionner un logo'))
        ->add('submit', SubmitType::class, array('label' =>'Enregistrer'))
        ->getForm(); //methode qui consititue le formulaire
        //etablie la connexion entre l'objet Form et l'objet Request
        $form->handleRequest($req);

        //methode permetant de savoir si le formualire a été envoyé
        //équivalent de de $request->getMethod() == POST lorsqu'on utilise
        if ($form->isSubmitted() && $form->isValid()) {
          //hydratation automatique de l'objet Producer grace à getData() retourne un object hydraté
          $producer = $form->getData();
          $file = $producer->getLogo(); //recuperation de l'objet uploadfile
          //placé dans la propriété logo $producer
          $filename = 'logo_'.strtolower($producer->getName()). '.' . $file->guessExtension();

          //getParameter('key') recup la valeur d'une clé définie dans config.yml
          //move(dossier_de_destination, nom_du_fichier)
          $file->move($this->getParameter('dir_logo'), $filename);

          //mise à jour de la propriéte logo
          $producer->setLogo($filename);

          //validation des donnee
          //validation de données en PHP sans annotations
       $str_len = strlen($producer->getName());
       $min = 3;
       $max = 10;
       $cond1 = $str_len >= $min;
       $cond2 = $str_len <= $max;
       $total_cond = $cond1 && $cond2;
       $message = "Le nom du producteur doit avoir";
       $message .= " au moins " . $min . " caractères";
       $message .= " et au plus " . $max . " caractères";

       if (!$total_cond) return new Response($message);
            if (!$total_cond) {
              return new Response(
                "Le nom du producteur doir avoir au moins". $min."caractères et au plus". $max ."caractères"
              );
            }

          $em = $this->getDoctrine()->getManager();
          $em->persist($producer);
          $em->flush();
          return $this->redirectToRoute('producer_index');
        }

        return $this->render('AppBundle:Producer:add.html.twig', array(
          'form' => $form->createView(), //envoyer le formulaire à la vue
          'message' => 'Simple texte',
          'color' => 'blue',
        ));
    }

    /**
     * @Route("/edit")
     */
    public function editAction()
    {
        return $this->render('AppBundle:Producer:edit.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("/delete")
     */
    public function deleteAction()
    {
        return $this->render('AppBundle:Producer:delete.html.twig', array(
            // ...
        ));
    }
    /**
    * @route("/{id}", name="producer_details")
    */
    public function producerDetailsAction($id) {
      //récupérer l'objet à partir de son identifiant ($id)
      $producer = $this->getDoctrine()->getRepository(Producer::class)->find($id);

      return $this->render('fruit/producer_details.html.twig', array(
        'producer' => $producer
      ));

    }



}
