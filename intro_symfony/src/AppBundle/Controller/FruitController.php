<?php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request; //change les requetes http en objet
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Fruit;
use AppBundle\Entity\Producer;
use AppBundle\Entity\Category;


/**
 * @Route("/fruits")
 */
class FruitController extends Controller
{
  /**
   * @Route("/", name="fruit_admin_page")
   */
   public function indexAction(Request $request) {
     //var_dump($request);
     $post = $request->request;
     //echo $post->get('name'); // echo $_POST['name']
     //echo $request->request->get('origin');

     if ($request->getMethod() == 'POST') {

       $name = $post->get('name');
       $origin = $post->get('origin');
       $comestible = $post->get('comestible');
       $producer = $post->get('producer'); // on obtient l'objet Producer complet et hydraté
       // on obtient un tableau d'identifiant
       // exemple = ["1", "4"] correspondant au checkbox cochées
       $categories_post = $post->get('categories');

       //récuperer l'objet producer complet à partir d'un id
       $producer = $this->getDoctrine()
        ->getRepository(Producer::class)->find($producer); //find() trouve l'id par défaut

           //verification du contenu de la variable comestible
           $comestible = ($comestible) ? 1 : 0;

       $fruit = new Fruit();



       //hydratation
       $fruit->setName($name);
       $fruit->setOrigin($origin);
       $fruit->setComestible($comestible);
       $fruit->setProducer($producer);
       if ($categories_post != NULL) //l'utilisateur a coché au moins une case
       {
         foreach ($categories_post as $c) {
         //A chaque passage création d'un objet de type catégorie
          $category = $this->getDoctrine()
           ->getRepository(Category::class)->find($c);
           //alimentation de la propriété catégory  de l'object $fruit
           $fruit->addCategory($category);
          }
       //utilisation du EntityManager
       $em = $this->getDoctrine()->getManager();
       $em->persist($fruit); //prépare la requete d'insertion
       //mais n'execute aucune requete SQL
       $em->flush(); //éxécute toutes les requete SQL en attente
       } // fin de if

      }

      //récupération des fruits
      // Fruit::class retourne chemin + nom de la classe
      //getRe
      $fruits = $this
        ->getDoctrine()
        ->getRepository(Fruit::class)
        ->findAll();

        //récuperation des producteurs
      $producers = $this
        ->getDoctrine()
        ->getRepository(Producer::class)
        ->findNotAssigned();

        //récuperation des cateorgies
      $categories= $this
        ->getDoctrine()
        ->getRepository(Category::class)
        ->findAll();

     // suite des opérations: créer un objet fruit, le transmettre à un
     // manager pour l'enregistrement en db

     return $this->render('fruit/index.html.twig', array(
       'fruits' => $fruits,
       'producers' => $producers,
       'categories' => $categories
     ));
   }

   /**
   * @Route("/delete/{id}", name="fruit_delete")
   */
   public function deleteAction($id) {
     //la variable $id correspond au parametre {$id}
     //definit au niveau de l'annotation @route

     $fruit = $this->getDoctrine()->getRepository(Fruit::class)->find($id);
     $em = $this->getDoctrine()->getManager();
     $em->remove($fruit); // cet methode ne produit pas requete SQL "en attente"
     $em->flush(); // execute les requete SQL en attente

      return $this->redirectToRoute('fruit_admin_page');
   }

   /**
   * @route("/update/{id}", name="fruit_update")
   */
   public function updateAction($id, Request $request) {
     //dans cette variante l'objet Fruit es crée sans le notifier au manager
      //$fruit->getDoctrine()->getRepository(Fruit::class)->find($id);

      //appeler de getRepository() depuis getManager() etabli
      //une visibilité entre le repository et le manager
      //ici le manager est notifier de l'existance de l'objet Fruit
      //si c'est objet change (reçoit de nouvelles valeur le manager le sait)
      // le manager le sait, le manager suveille cet objet.
      $em = $this->getDoctrine()->getManager();
      $fruit = $em->getRepository(Fruit::class)->find($id);

      if ($request->getMethod() == 'POST') {
        $fruit->setName($request->request->get('name'));
        $fruit->setOrigin($request->request->get('origin'));

        $comestible = ($request->request->get('comestible')) ? 1 : 0;
        $fruit->setComestible($comestible);

        $em->flush(); //L 'OBJET FRUIT A REÇU DE NOUVELLES VALEURS, le manager éxécutera  la requete SQL appropriéé

        return $this->redirectToRoute('fruit_admin_page');
      }

        return $this->render('fruit/update.html.twig', array(
         "fruit" => $fruit
        ));
   }

   /**
   * @route("/{id}", name="fruit_details")
   */
   public function detailsAction($id) {
     //récupérer l'objet à partir de son identifiant ($id)
     $fruit = $this->getDoctrine()->getRepository(Fruit::class)->find($id);

     return $this->render('fruit/details.html.twig', array(
       'fruit' => $fruit
     ));
   }

   /**
   * @route("/category/{name}", name="list_categories")
   */
   public function listAction($name){
       $fruits = $this
         ->getDoctrine()
         ->getRepository(Fruit::class)
         ->findByCategoryName($name);

       return $this->render('fruit/list_categories.html.twig', array(
         'fruits' => $fruits,
         'name' => $name
       ));
   }

   /**
   * @route("/api/json")
   */
  public function jsonAction(){
    $fruits = ['pomme', 'poire', 'cerise'];
    $fruit  = ['name' => 'Pomme',
               'origin' => 'France',
               'comestible' => true,
               'category' => [
                array('name' => 'Cuisine'),
                array('name' => 'Voyage')
                ]
              ];
    $fruits_json = json_encode($fruits);
    $fruit_json = json_encode($fruit);

      return new Response($fruit_json);
  }

  /**
  * @route("/api/client")
  */
  public function clientAction() {
    return $this->render('client.html.twig');
  }

  /**
  * @route("/api/list")
  */
  public function ajaxListFruitsAction() {
    $fruits =  $this->getDoctrine()
                  ->getRepository(Fruit::class)
                  ->findAll();
    //tentative d'necodege en JSON, json_encode() ne peut pas encoder des objet Php
    //json_encode ne fonctionne qu'avec des tableau assoc
    //$fruits_json = json_encode($fruits);
      $fruits_assoc = [];
    //Transformation de l'objet Fruit en tableau assos
    foreach ($fruits as $fruit) {
      $fruit_assoc = [
        'id' => $fruit->getId(),
        'name' => $fruit->getName(),
        'origin' => $fruit->getOrigin(),
        'comestible' => $fruit->getComestible(),
        'like' => $fruit->getLike(),
      ];
      if ($fruit->getProducer()) {
        $fruit_assoc['producer'] = $fruit->getProducer()->getName();
      }

      $fruits_assoc[] = $fruit_assoc; //équivalent d'un array_push
    }

    $fruits_json = json_encode($fruits_assoc);
      return new Response($fruits_json);
    }

  /**
  * @route("/api/detail/{id}")
  */
  public function ajaxDetailFruitAction($id) {
    $fruit = $this->getDoctrine()
                  ->getRepository(Fruit::class)
                  ->find($id);

    //transformation de l'objet en tableau associatif
    $fruit_assoc = [
      'id' => $fruit->getId(),
      'name' => $fruit->getName(),
      'origin' => $fruit->getOrigin(),
    ];
    $comestible = ($fruit->getComestible()) ? 'Oui' : 'Non';
    $fruit_assoc['comestible'] = $comestible;

    //producer
    if ($fruit->getProducer()) {
    $p = $fruit->getProducer();
       $producer_assoc = [
         'id' => $p->getId(),
         'name' => $p->getName(),
       ];
       $producer_assoc['email'] = ($p->getEmail()) ? $p->getEmail() : '';
       $producer_assoc['logo'] = ($p->getLogo()) ? $p->getLogo() : '';
    } else {
      $producer_assoc['producer'] = null;
    }

    $fruit_assoc['producer'] = $producer_assoc;

    //détaillant (tableau d'objet Retailor)
    if (sizeof($fruit->getRetailors()) > 0) {
      $retailors = [];
      foreach ($fruit->getRetailors() as $r) {
        $retailors[] = ['id' => $r->getId(), 'name' => $r->getName()];
      }
      $fruit_assoc['retailors'] = $retailors;
    } else {
      $fruit_assoc['retailors'] = null;
    }

    //catégorie
    if (sizeof($fruit->getCategory()) > 0) {
      $categories = [];
      foreach ($fruit->getCategory() as $c) {
        $categories[] = ['id' => $c->getId(),
                         'name' => $c->getName()
                       ];
      }
      $fruit_assoc['categories'] = $categories;
    } else {
      $fruit_assoc['categories'] = null;
    }


    $fruit_json = json_encode($fruit_assoc);
    return new Response($fruit_json);
  }

  /**
  * @route("/api/post")
  */
  public function ajaxPostAction(Request $request) {
    $name = $request->request->get('name');
    $sport = $request->request->get('sport');
    return new Response($name. ' est un expert en '.$sport[1]['name']);
  }

  /**
  * @route("/api/like")
  */
  public function incrementLikeAction(Request $request) {
    $id = $request->request->get('id');
    $like = $request->request->get('like');


    return new Response($nb_like);
  }


}

?>
