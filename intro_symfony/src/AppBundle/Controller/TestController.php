<?php
namespace AppBundle\Controller; //indique le chemin
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route; // classe qui gère l'annotation
use Symfony\Bundle\FrameworkBundle\Controller\Controller; // use est comme un include
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Classe\Fruit;


  /**
   * @Route("/test")
   */
 class TestController extends Controller {
   private  $message  = 'message';
   private  $legumes  = ['carotte', 'tomate', 'lentille'];
   private  $fruits   = array(
     array('name' => 'Mangue',
           'origin' => 'Amerique latine',
           'comestible' => true),
     array('name' => 'Banane',
           'origin' => 'Guadeloupe',
           'comestible' => true),
     array('name' => 'Annaze',
           'origin' => 'Tchernobyl',
           'comestible' => false),
      );
      //ERREUR : instanciation prematurée
      //on ne peut pas instancier la classe fruit à cet endroit. il faut se placer dans le constructeur
      //pour povoir faire cette instanciation
  //  private $fruits2 = array(
  //    new Fruit('Orange', 'Sicile', true),
  //    new Fruit('Tromate', 'Suceava', false),
  //    new Fruit('Citron', 'Bari', true),
  //  );
    private $fruits2; //déclaration saans assignation
    // la déclaration se fera dans le __construct()

   public function __construct() {
     $this->fruits2 = array(
       new Fruit('Orange', 'Sicile', true, 'https://fr.wikipedia.org/wiki/Orange'),
       new Fruit('Tromate', 'Suceava', false, NULL ),
       new Fruit('Citron', 'Bari', true,'https://fr.wikipedia.org/wiki/Citron'),
     );
   }

   public function getMessage() {
     return $this->message;
   }

   private function getLegumeList() {
     $output = "<ul>";

     foreach ($this->legumes as $legume) {
       $output .= '<li>'. $legume .'</li>';
     }
     $output .= "</ul>";
     return $output;
   }

   /**
   * @route("/example" name="example_page")
   */
  //  public function exampleAction() {
  //    //return 'jey'; //retour non valide "string"  in faut retourner
  //    //un objet de type Response
  //    $reponse  = new Response("toto");
  //    $reponse2 = new Response('<h1>TOTO</h1>');
  //    $reponse3 = new Response($this->getMessage());
  //    //$reponse4 = new Response($this->legumes); //ERROR : on ne peut retourner au client
  //    //une structure php non convertible en String
  //    $reponse5 = new Response($this->getLegumeList());
   //
  //    return $this->render('test/example.html.twig', array('fruits' => $this->fruits2));
  //  }
   /**
   *  @route("/legumes/list")
   */
   public function LegumeListAction(){
     $res = new Response($this->getLegumeList());
     return $res;
   }
   /**
   *  @route("/legume/static")
   */
   public function LegumeStaticAction(){
     //renvoi un fichier statique
     return $this->render('test/legumes.html'); // render() va direct dans views
     //on fait le chemin à partirdu sous dossier views
   }

   /**
   *  @route("/legume/dynamic", name="fruits_page")
   */
   public function LegumeDynamicAction(){
     //renvoi un fichier dynamique
     //le deuxième argument de la methode render() est obligatoirement un tableau assos
     //permettant d'envoyer un a View des données aussi bien simple(string, number etc.) que complexe(array, object)
     return $this->render('test/legumes.html.twig', array(
       'title' => 'Liste de legumes',
       'message'  => $this->getMessage(),
       'legumes'  => $this->legumes, //renvoi un tableau indexé numerique
       'fruits'   => $this->fruits,
       'fruits2'  => $this->fruits2, //renvoi un tableau d'objet de type Fruit
       'toto'     => NULL
     )); // render() va direct dans views
     //on fait le chemin à partirdu sous dossier views
   }
   /**
  * @Route("/fruits-comestibles", name="fruits_comestibles_page")
 */
 public function fruitsComestiblesAction() {
   return $this->render('test/fruits-comestibles.html.twig', array(
     'fruits' => $this->fruits2
   ));
 }


 }
?>
