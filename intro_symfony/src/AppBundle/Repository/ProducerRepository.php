<?php

namespace AppBundle\Repository;

/**
 * ProducerRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ProducerRepository extends \Doctrine\ORM\EntityRepository
{
  public function findAllOrderedByName()
  {
    // le DQL ne s'utilise que dans le Repository
    //requete DQL(=Doctrine Query Language + un touche d'objet)(retourne des objets)
      return $this->getEntityManager()
      ->createQuery('SELECT p FROM AppBundle:Producer p ORDER BY p.name ASC')
      ->getResult(); //se charge de mettre les resultat sous forme de tableau d'objet
  }

  public function findNotAssigned()
  {

      return $this->getEntityManager()
        ->createQuery(
          'SELECT p FROM AppBundle:Producer p
           WHERE NOT EXISTS (SELECT f FROM AppBundle:Fruit f WHERE p = f.producer)
           ORDER BY p.name ASC')
        ->getResult();
  }
}
